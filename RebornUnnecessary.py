# Reborn Maintenance App Trigger Handlers
# Created by Azaiel for RebornOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# RebornOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Unnecessary Packages")
try:
    import httplib
except:
    import http.client as httplib

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. The following operations will not work").show()

workingDirectory = os.path.dirname(os.path.realpath(__file__))

# Create Handlers (Triggers) for each item
class Handler:

# Close the window
    def onDestroy2(self, *args):
        Gtk.main_quit()

################################################################################
############################### Buttons ########################################
################################################################################

# Remove Packages
    def clickRemovePackages(self, button):
        os.system('xterm -e sudo pacman -Rdd - < /tmp/package-files.txt --noconfirm')
        #os.system('xterm -e sudo pacman -Rs $(pacman -Qqdt) --noconfirm')
        Notify.Notification.new("Unnecessary Packages Removed").show()

# Edit Package List
    def clickRemovePackages2(self, button):
        os.system('xdg-open /tmp/package-files.txt')
        Notify.Notification.new("The packages shown in the Reborn Updates and Maintenance window will not been updated. However, your changes here will be applied").show()

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(workingDirectory + "/RebornMaintenance.glade")
builder.connect_signals(Handler())

# =====================================
# View the file when the window pops up
# =====================================
with open('/tmp/package-files.txt', 'r') as pacFile:
    data = pacFile.read()
    builder.get_object("fileContentsView").get_buffer().set_text(data)
# =====================================

window1 = builder.get_object("Reborn2")
window1.show_all()

Gtk.main()

Notify.uninit()
